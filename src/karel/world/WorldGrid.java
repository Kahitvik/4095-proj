/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package karel.world;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JScrollBar;
import karel.world.objects.Direction;
import karel.world.objects.Robot;
import karel.world.objects.Room;
import karel.world.objects.Rooms;

/**
 *
 * @author Ryan Ball
 */
public class WorldGrid extends javax.swing.JPanel {

    private JScrollBar verticalBar;
    private JScrollBar horizontalBar;
    
    private int pHeight;
    private int pWidth;
    
    private Rooms<Integer,Integer,Room> rooms;
    private Robot robot;

    private int cellSize;
    private int dotRadius;
    private int dotOffset;
    private int xOffset;
    private int yOffset;
    private double moveOffset = 0;
    private double turnOffset = 0;
    
    // bottom-most row number and left-most column number
    private int rowNumber = 1;
    private int colNumber = 1;
    
    private int fontHeight;
    private FontMetrics fontMetrics; 
    
    public WorldGrid() {
        this(new Rooms<Integer,Integer,Room>(), new JScrollBar(), new JScrollBar(), 50);
    }
    
    /**
     * Creates new form WorldGrid
     */
    public WorldGrid(Rooms<Integer,Integer,Room> rooms,
            JScrollBar verticalBar, JScrollBar horizontalBar, int cellSize) {
        
        this.cellSize = cellSize;
        this.rooms = rooms;
        this.verticalBar = verticalBar;
        this.horizontalBar = horizontalBar;
        dotRadius = cellSize / 6;
        dotOffset = dotRadius/2;
        initComponents();
    }
    
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);

        pHeight = this.getHeight();
        pWidth = this.getWidth();
		
        Graphics2D g2 = (Graphics2D) g;
        g2.setFont(new Font("Sans Serif", Font.BOLD, cellSize/4 + 1));
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        fontMetrics = g2.getFontMetrics();
        fontHeight = fontMetrics.getHeight();
    
        rowNumber = verticalBar.getMaximum() - verticalBar.getValue();
        colNumber = horizontalBar.getValue();
        
        yOffset = rowNumber % cellSize;
        xOffset = colNumber % cellSize;
        
        rowNumber /= cellSize;
        colNumber /= cellSize;
        rowNumber++;
        colNumber++;
        
        int tempColNumber = colNumber;
        int tempRowNumber = rowNumber;

        // Render Grid
        for(int x = -xOffset; x <= pWidth; x+=cellSize, tempColNumber++){
                tempRowNumber = rowNumber;
                for(int y = pHeight+ yOffset; y >= 0; y-=cellSize, tempRowNumber++) {
                        if (rooms != null) {
                            Room room = rooms.get(tempRowNumber,tempColNumber);
                            if (room != null) {
                                
                                drawWalls(g2, room, x, y);
                                if (room.getNumOfBeepers() != 0) {
                                    drawBeeperStack(g2, room, x, y);
                                }
                            }
                        }

                        // draw grid dot
                        g2.setColor(Color.black);
                        g2.fillOval(x-dotOffset, y-dotOffset, dotRadius, dotRadius); 
                }			
        }
        // draw robot if it exists
        if (robot != null) {
            g2.setStroke(new BasicStroke(3));
            int col = robot.getCol();
            int row = robot.getRow();
            // draw robot if it is located within the bounds of the current display
            if (col >= colNumber && col < tempColNumber &&
                row >= rowNumber && row < tempRowNumber) {

                int x = Math.abs(robot.getCol() - colNumber) * cellSize - xOffset;
                int y = pHeight + yOffset - Math.abs(robot.getRow() - rowNumber) * cellSize;
                robot.draw(g2, x, y, moveOffset, turnOffset, cellSize);
            }
        }
    }
    
    public void drawWalls(Graphics2D g2, Room room, int x, int y) {

        g2.setColor(Color.red);
        g2.setStroke(new BasicStroke(cellSize/10));
        if (room.getWall(Direction.EAST)) {
            g2.drawLine(x + cellSize, y, x + cellSize, y - cellSize);
        }
        if (room.getWall(Direction.WEST)) {
            g2.drawLine(x, y, x, y - cellSize);
        }
        if (room.getWall(Direction.NORTH)) {
            g2.drawLine(x, y - cellSize, x + cellSize, y - cellSize);
        }
        if (room.getWall(Direction.SOUTH)) {
            g2.drawLine(x, y, x + cellSize, y);
        }
    }
    
    public void drawBeeperStack(Graphics2D g2, Room room, int x, int y) {
        // draw beeper oval
        int xLoc = (int) (x + cellSize / 4.0);
        int yLoc = (int) (y - cellSize * 3.0/4.0);
        int cellHalved = (int) (cellSize/2.0);
        g2.setColor(new Color(0, 150, 150));
        g2.fillOval(xLoc, yLoc, cellHalved, cellHalved);

        // draw beeper number
        int numberOfBeepers = room.getNumOfBeepers();
        g2.setColor(Color.yellow);
        String beeperString = String.valueOf(numberOfBeepers);
        int stringPixelLength = fontMetrics.stringWidth(beeperString);
        g2.drawString(beeperString, (int)(x + cellSize/2.0 - stringPixelLength/2.0),
                (int)(y - cellSize/2.0 + fontHeight/4.0));
    }
    
    public void setRooms(Rooms<Integer,Integer,Room> rooms) {
        this.rooms = rooms;
    }
    
    public void setRobot(Robot robot) {
        this.robot = robot;
    }
    
    public void moveRobot(final int speed) throws RobotMoveException {
        
        Direction dir = robot.getDirection();
        int robotRow = robot.getRow();
        int robotCol = robot.getCol();
        
        Room room = rooms.get(robotRow, robotCol);

        String errorMessage = "Robot ran into wall and is now turning off.";
        
        switch(dir) {
            case NORTH:
                
                if (room != null  && room.getWall(dir)) {
                    throw new RobotMoveException(errorMessage);
                }
                
                animateMove(dir, speed);
                
                if (robotRow + 1 == rowNumber + pHeight/cellSize) {
                    verticalBar.setValue(verticalBar.getValue() - cellSize);
                }
                break;
            case SOUTH:
                
                if (room != null  && room.getWall(dir)) {
                    throw new RobotMoveException(errorMessage);
                }
                
                animateMove(dir, speed);
                
                if (robotRow - 1 == rowNumber) {
                    verticalBar.setValue(verticalBar.getValue() + cellSize);
                }
                break;
            case WEST:
                
                if (room != null  && room.getWall(dir)) {
                    throw new RobotMoveException(errorMessage);
                }
                
                animateMove(dir, speed);
                
                if (robotCol - 1 == colNumber) {
                    horizontalBar.setValue(horizontalBar.getValue() - cellSize);
                }
                break;
            case EAST:
                
                if (room != null  && room.getWall(dir)) {
                    throw new RobotMoveException(errorMessage);
                }
                
                animateMove(dir, speed);
                
                if (robotCol + 1 == colNumber + pWidth/cellSize) {
                    horizontalBar.setValue(horizontalBar.getValue() + cellSize);
                }
                break;   
        }
    }
    
    private void animateMove(Direction dir, int speed) {
        
        double distance = cellSize/15;
        int mspf = speed/15;    // milliseconds per frame
        
        for (moveOffset = 0; moveOffset <= cellSize; moveOffset+=distance) {
            try {
                Thread.sleep(mspf);
            } catch (InterruptedException ex) {
                assert false;
            }
            this.repaint();
        }
        moveOffset = 0;

        robot.move(dir);
    }
    
    public void turnRobot(int speed) {
        double angle = 90/15;
        int mspf = speed/15;    // milliseconds per frame
        
        for (turnOffset = 0; turnOffset <= 90; turnOffset+=angle) {
            try {
                Thread.sleep(mspf);
            } catch (InterruptedException ex) {
                assert false;
            }
            this.repaint();
        }
        turnOffset = 0;
        robot.turnleft();
    }
    
    public void setCellSize(int cellSize) {
        this.cellSize = cellSize;
        dotRadius = cellSize / 6;
        dotOffset = dotRadius/2;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
